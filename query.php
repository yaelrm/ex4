<?php
class Query {
    protected $db; //database connection
    function __construct($dbc) { //initialization
        $this->dbc = $dbc;
    }
    
    public function query($query){ //run a query in the database
        if($result = $this->dbc->query($query)){
            return $result;
        } else {
            echo "Error running the query";
        }
    }
}
?>