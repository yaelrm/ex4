<?php
include "books.php";
include "query.php";

  // database connection
$db = new DB('localhost', 'intro', 'root', '');
$dbc =$db->connect();

$query = new Query($dbc);
$q = "SELECT * FROM books INNER JOIN users ON books.users_fk = users.id";
$result = $query->query($q);
echo '<br>';
if($result->num_rows > 0){
    echo '<table>';
    echo '<tr><th>Book</th><th>User</tr>';
    while($row = $result->fetch_assoc()){
        echo '<tr>';
        echo '<td>'.$row['title_field'].'</td><td>'.$row['name'].'</td>';
        echo '</tr>';
    }
    echo '</table>';
} else {
    echo "Sorry no results";
}
?>

